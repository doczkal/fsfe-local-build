# FSFE Local Build

This is a collection of scripts and configuration files necessary to build fsfe.org websites locally. 

Full instructions can be found in FSFE's wiki: https://wiki.fsfe.org/TechDocs/Mainpage/BuildLocally
